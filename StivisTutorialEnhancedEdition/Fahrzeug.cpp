#include "stdafx.h"
#include "Fahrzeug.h"



Fahrzeug::Fahrzeug(string Antrieb, string Steuerung, double Ladekapazitaet, double Gewicht, array<double, 3> Position, bool Bemannt) //constructor*
{
	this->Antrieb = Antrieb;
	this->Steuerung = Steuerung;
	this->Ladekapazitaet = Ladekapazitaet;
	this->Gewicht = Gewicht;
	this->Position = Position;
	this->Bemannt = Bemannt;
	this->an = false;
}
Fahrzeug::~Fahrzeug() //Destructor
{}
void Fahrzeug::setAntrieb(string Antrieb)
{
	this->Antrieb = Antrieb;
}
string Fahrzeug::getAntrieb()
{
	return Antrieb;
}
void Fahrzeug::setSteuerung(string Steuerung)
{
	this->Steuerung = Steuerung;
}
string Fahrzeug::getSteuerung()
{
	return Steuerung;
}
void Fahrzeug::setLadekapazitaet(double Ladekapazitaet)
{
	this->Ladekapazitaet = Ladekapazitaet;
}
double Fahrzeug::getLadekapazitaet()
{
	return Ladekapazitaet;
}
void Fahrzeug::setGewicht(double Gewicht)
{
	this->Gewicht = Gewicht;
}
double Fahrzeug::getGewicht()
{
	return Gewicht;
}
void Fahrzeug::setPosition(array<double, 3> Position)
{
	this->Position = Position;
}
array<double, 3> Fahrzeug::getPosition()
{
	return Position;
}
void Fahrzeug::setBemannt(bool Bemannt)
{
	this->Bemannt = Bemannt;
}
bool Fahrzeug::getBemannt()
{
	return Bemannt;
}
bool Fahrzeug::laeuft()
{
	return this->an;
}
