#pragma once
#include<iostream>
#include<array>
#include <string>

using namespace std;

class Fahrzeug
{
protected:
	string Antrieb;
	string Steuerung;
	double Ladekapazitaet;
	double Gewicht;
	array<double, 3> Position;
	bool Bemannt;
	bool an;

public: 
	Fahrzeug(string Antrtieb, string Steuerung, double Ladekapazitaet, double Gewicht, array<double, 3> Position, bool Bemannt); //constructor
	~Fahrzeug(); //Destructor
	void setAntrieb(string Antrieb);
	string getAntrieb();
	void setSteuerung(string Steuerung);
	string getSteuerung();
	void setLadekapazitaet(double Ladekapazitaet);
	double getLadekapazitaet();
	void setGewicht(double Gewicht);
	double getGewicht();
	void setPosition(array<double, 3> Position);
	array<double, 3> getPosition();
	void setBemannt(bool Bemannt);
	bool getBemannt();
	bool laeuft();
	virtual bool starten() = 0;//Mutterklasee wird Abstrakt, d.h. es kann kein Objekt Fahrzeug erstellt werden, virtuelle Methode wird in abgeleiter Klasse redefiniert
	virtual bool stoppen() = 0;




};

