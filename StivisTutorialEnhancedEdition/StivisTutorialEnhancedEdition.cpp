#include "stdafx.h"
#include <iostream>
#include "Fahrzeug.h"
#include "Raumfahrzeug.h"
using namespace std;

int main()
{
	Raumfahrzeug * meinFahrzeugPtr = new Raumfahrzeug("V12", "Lenkrad", 5.0, 1000.0001, { 1,2,3 }, true);
	Raumfahrzeug  meinFahrzeug = *(new Raumfahrzeug("V8", "Lenkrad", 5.0, 1000.0001, { 1,2,3 }, true));
	cout << meinFahrzeugPtr->getAntrieb() << endl;
	cout << meinFahrzeug.getAntrieb() << endl;
	return 0;
}

