#pragma once
#include "Fahrzeug.h"

class Raumfahrzeug:public Fahrzeug
{
public:
	Raumfahrzeug(string Antrtieb, string Steuerung, double Ladekapazitaet, double Gewicht, array<double, 3> Position, bool Bemannt); //constructor
	bool starten();
	bool stoppen();


};
